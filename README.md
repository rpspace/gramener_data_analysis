## Conda setup

#1:Download & install Miniconda from https://conda.io/miniconda.html

#2: Run following commonds on terminal
1. bash Miniconda3-latest-Linux-x86_64.sh 
2. conda create -n taskg python=3
3. source activate taskg
4. Under conda environment run below commnad
5. pip install -r requirements.txt

#Or run followings commands manually
pip install ipython
pip install matplotlib
pip install pandas
pip install flask
pip install mpld3
source activate taskg

#Start Server Using
python main.py 

##For Analysis story goto story.md
#Note: Default url:http://127.0.0.1:5000/home

_______________

## Steps in analysis
1: Read given csv file as  data = pd.read_csv('static/usecase/nas-pupil-marks.csv')
2: Define function to visualise each usecase problems with particular url 
3: Display visual on web pages 
4: Use plt.savefig('abc.png') to save graph plot in png 



