import matplotlib
matplotlib.use('Agg')
from flask import Flask, render_template
import json
import pandas as pd
#import matplotlib
import matplotlib.pyplot as plt
import mpld3

data = None
def get_data():
    global data
    data = pd.read_csv('static/usecase/nas-pupil-marks.csv')
    #data1 = pd.read_csv('static/usecase/nas-pupil-marks.csv', header=None, chunksize=500)
    #data = pd.concat(data1, ignore_index=True)

app = Flask(__name__)

get_data()
@app.route('/')
def index():
    return render_template("index.html")

@app.route('/home')
def index2():

    return render_template("index.html")


@app.route('/home/student')
def get_per():
    #import pdb;pdb.set_trace()
    # select avg(Maths %', 'Science %', 'Reading %', 'Social %) from data group by Mother edu
    mperf = data.groupby(['Mother edu'])['Maths %', 'Science %', 'Reading %', 'Social %' ].mean()
    # select avg(Maths %', 'Science %', 'Reading %', 'Social %) from data group by Father edu
    fperf = data.groupby(['Father edu'])['Maths %', 'Science %', 'Reading %', 'Social %' ].mean()
    # select avg(Maths %', 'Science %', 'Reading %', 'Social %) from data group by Father occupation
    father_occu = data.groupby(['Father occupation'])['Maths %', 'Science %', 'Reading %', 'Social %' ].mean()
    # select avg(Maths %', 'Science %', 'Reading %', 'Social %) from data group by Mother occupation
    mother_occu = data.groupby(['Mother occupation'])['Maths %', 'Science %', 'Reading %', 'Social %' ].mean()
    fig = plt.figure()
    plt.plot(mperf)
    plt.legend(["Maths %", "Science %", "Reading %", "Social %"], loc='best')
    plt.ylabel(' % marks')
    plt.xlabel("Education Level: 0: Not applicable  1: Illiterate  2: Primary  3: Sr secondary    4: Secondary  5: Degree & above")
    plt.title("Mother Education")

    motheredu_fig = mpld3.fig_to_html(fig, template_type="simple")
    fig2 = plt.figure()
    plt.plot(fperf)
    plt.legend(["Maths %", "Science %", "Reading %", "Social %"], loc='best')
    plt.ylabel(' % marks')
    plt.xlabel("Education Level: 0: Not applicable  1: Illiterate  2: Primary  3: Sr secondary    4: Secondary  5: Degree & above")
    plt.title("Father Education")

    fatheredu_fig = mpld3.fig_to_html(fig2, template_type="simple")
    fig3 = plt.figure()
    plt.plot(father_occu)
    plt.legend(["Maths %", "Science %", "Reading %", "Social %"], loc='best')
    plt.ylabel(' % marks')
    plt.xlabel("Education Level: 0: Not applicable  1: Illiterate  2: Primary  3: Sr secondary    4: Secondary  5: Degree & above")
    plt.title("Father Occupation")

    fatheroccu_fig = mpld3.fig_to_html(fig3, template_type="simple")
    fig4 = plt.figure()
    plt.plot(mother_occu)
    plt.legend(["Maths %", "Science %", "Reading %", "Social %"], loc='best')
    plt.ylabel(' % marks')
    plt.xlabel("Education Level: 0: Not applicable  1: Illiterate  2: Primary  3: Sr secondary    4: Secondary  5: Degree & above")
    plt.title("Mother Occupation")
    

    motheroccu_fig = mpld3.fig_to_html(fig4,template_type="simple")
    plt.close()

    return render_template("perf.html", motheredu_fig=motheredu_fig,
                            fatheredu_fig=fatheredu_fig,
                            fatheroccu_fig=fatheroccu_fig,
                            motheroccu_fig=motheroccu_fig)


@app.route('/home/gender')
def get_genderperf():
    #import pdb;pdb.set_trace()
    #SELECT * FROM data WHERE Gender =  Boy;
    boy_data = data[data['Gender'] == 1]
    #SELECT AVG(Maths %', 'Science %', 'Reading %', 'Social %') FROM boy_data GROUPBY State;
    boy_perf = boy_data.groupby(['State'])['Maths %', 'Science %', 'Reading %', 'Social %'].mean()
    
    fig1 = plt.figure()
    plt.plot(boy_perf)
    plt.legend(["Maths %", "Science %", "Reading %", "Social %"], loc='best')
    plt.ylabel(' % marks ')
    plt.xlabel(" States")
    boy_fig = mpld3.fig_to_html(fig1, template_type="simple")
    #SELECT * FROM data WHERE Gender =  Girl;
    girl_data = data[data['Gender'] == 2]
    #SELECT AVG(Maths %', 'Science %', 'Reading %', 'Social %') FROM girl_data GROUPBY State;
    girl_perf = girl_data.groupby(['State'])['Maths %', 'Science %', 'Reading %', 'Social %' ].mean()
    fig2 = plt.figure()
    plt.plot(girl_perf)
    plt.legend(["Maths %", "Science %", "Reading %", "Social %" ],loc='best')
    plt.ylabel(' % marks ')
    plt.xlabel(" States")
    
    girl_fig = mpld3.fig_to_html(fig2, template_type="simple")
    plt.close()

    return render_template("genderperf.html", boy_fig=boy_fig, girl_fig=girl_fig)


@app.route('/home/state')
def get_stateperf():

    #import pdb;pdb.set_trace()
    southdata = data.loc[data['State'].isin(['KA','TN', 'AP','KL'])]
    perf = southdata.groupby(['State'])['Maths %', 'Science %'].mean()
    fig = plt.figure()
    plt.plot(perf)
    plt.legend(["Maths %", "Science %"], loc='best')
    plt.ylabel(' % marks ')
    plt.xlabel(" States")
    plt.title("Student performace in South India")

    southstate_fig = mpld3.fig_to_html(fig, template_type="simple")
    northdata = data.loc[~data['State'].isin(['KA','TN', 'AP','KL'])]
    perf = northdata.groupby(['State'])['Maths %', 'Science %'].mean()
    fig2 = plt.figure()
    plt.plot(perf)
    plt.legend(["Maths %", "Science %"], loc='best')
    plt.ylabel(' % marks ')
    plt.xlabel(" States")
    plt.title("Student performace in rest of India ")
    

    northstate_fig = mpld3.fig_to_html(fig2, template_type="simple")
    plt.close()

    return render_template("stateperf.html", southstate_fig=southstate_fig, northstate_fig=northstate_fig)


if __name__ == '__main__':
    app.run(debug=True)
    
